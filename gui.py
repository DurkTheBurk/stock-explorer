import dearpygui.core as dpg
import dearpygui.simple as sdpg
from windows.dividend_calculator import dividend_calculator

def style_1():
    dpg.set_style_window_padding(4.00, 4.00)
    dpg.set_style_frame_padding(6.00, 4.00)
    dpg.set_style_item_spacing(6.00, 2.00)
    dpg.set_style_item_inner_spacing(5.00, 1.00)
    dpg.set_style_touch_extra_padding(0.00, 0.00)
    dpg.set_style_indent_spacing(6.00)
    dpg.set_style_scrollbar_size(18.00)
    dpg.set_style_grab_min_size(20.00)
    dpg.set_style_window_border_size(1.00)
    dpg.set_style_child_border_size(1.00)
    dpg.set_style_popup_border_size(1.00)
    dpg.set_style_frame_border_size(0.00)
    dpg.set_style_tab_border_size(0.00)
    dpg.set_style_window_rounding(0.00)
    dpg.set_style_child_rounding(0.00)
    dpg.set_style_frame_rounding(0.00)
    dpg.set_style_popup_rounding(0.00)
    dpg.set_style_scrollbar_rounding(0.00)
    dpg.set_style_grab_rounding(0.00)
    dpg.set_style_tab_rounding(0.00)
    dpg.set_style_window_title_align(0.50, 0.50)
    dpg.set_style_button_text_align(0.50, 0.50)
    dpg.set_style_selectable_text_align(0.00, 0.00)
    dpg.set_style_display_safe_area_padding(3.00, 3.00)
    dpg.set_style_global_alpha(1.00)
    dpg.set_style_antialiased_lines(True)
    dpg.set_style_antialiased_fill(True)
    dpg.set_style_curve_tessellation_tolerance(1.25)
    dpg.set_style_circle_segment_max_error(1.60)
    dpg.set_theme_item(dpg.mvGuiCol_TextDisabled, 102, 102, 102, 255)
    dpg.set_theme_item(dpg.mvGuiCol_WindowBg, 64, 64, 64, 255)
    dpg.set_theme_item(dpg.mvGuiCol_ChildBg, 64, 64, 64, 255)
    dpg.set_theme_item(dpg.mvGuiCol_PopupBg, 64, 64, 64, 255)
    dpg.set_theme_item(dpg.mvGuiCol_Border, 31, 31, 31, 181)
    dpg.set_theme_item(dpg.mvGuiCol_BorderShadow, 255, 255, 255, 15)
    dpg.set_theme_item(dpg.mvGuiCol_FrameBg, 107, 107, 107, 138)
    dpg.set_theme_item(dpg.mvGuiCol_FrameBgHovered, 107, 107, 107, 102)
    dpg.set_theme_item(dpg.mvGuiCol_FrameBgActive, 143, 143, 143, 171)
    dpg.set_theme_item(dpg.mvGuiCol_TitleBg, 48, 48, 48, 255)
    dpg.set_theme_item(dpg.mvGuiCol_TitleBgActive, 56, 56, 56, 255)
    dpg.set_theme_item(dpg.mvGuiCol_TitleBgCollapsed, 43, 43, 43, 230)
    dpg.set_theme_item(dpg.mvGuiCol_MenuBarBg, 85, 85, 85, 255)
    dpg.set_theme_item(dpg.mvGuiCol_ScrollbarBg, 61, 61, 61, 135)
    dpg.set_theme_item(dpg.mvGuiCol_ScrollbarGrab, 105, 105, 105, 255)
    dpg.set_theme_item(dpg.mvGuiCol_ScrollbarGrabHovered, 133, 133, 133, 255)
    dpg.set_theme_item(dpg.mvGuiCol_ScrollbarGrabActive, 194, 194, 194, 255)
    dpg.set_theme_item(dpg.mvGuiCol_CheckMark, 166, 166, 166, 255)
    dpg.set_theme_item(dpg.mvGuiCol_SliderGrab, 133, 133, 133, 255)
    dpg.set_theme_item(dpg.mvGuiCol_SliderGrabActive, 163, 163, 163, 255)
    dpg.set_theme_item(dpg.mvGuiCol_Button, 186, 186, 186, 89)
    dpg.set_theme_item(dpg.mvGuiCol_ButtonHovered, 133, 133, 133, 150)
    dpg.set_theme_item(dpg.mvGuiCol_ButtonActive, 194, 194, 194, 255)
    dpg.set_theme_item(dpg.mvGuiCol_Header, 97, 97, 97, 255)
    dpg.set_theme_item(dpg.mvGuiCol_HeaderHovered, 120, 120, 120, 255)
    dpg.set_theme_item(dpg.mvGuiCol_HeaderActive, 194, 194, 194, 196)
    dpg.set_theme_item(dpg.mvGuiCol_Separator, 0, 0, 0, 35)
    dpg.set_theme_item(dpg.mvGuiCol_SeparatorHovered, 179, 171, 153, 74)
    dpg.set_theme_item(dpg.mvGuiCol_SeparatorActive, 179, 171, 153, 172)
    dpg.set_theme_item(dpg.mvGuiCol_ResizeGrip, 66, 150, 250, 64)
    dpg.set_theme_item(dpg.mvGuiCol_ResizeGripHovered, 66, 150, 250, 171)
    dpg.set_theme_item(dpg.mvGuiCol_ResizeGripActive, 66, 150, 250, 242)
    dpg.set_theme_item(dpg.mvGuiCol_PlotHistogram, 48, 199, 51, 255)
    dpg.set_theme_item(dpg.mvGuiCol_TextSelectedBg, 186, 186, 186, 89)
    dpg.set_theme_item(dpg.mvGuiCol_NavHighlight, 66, 150, 250, 255)

def primary():
    with sdpg.window("Main Window"):
        style_1()
        dpg.set_item_color(f"Main Window", dpg.mvGuiCol_WindowBg, [22, 22, 22])
        dpg.set_main_window_title("SARFIS")
        dpg.enable_docking(shift_only=True)
        dpg.set_main_window_resizable(True)
        dpg.set_style_window_padding(0,0)
        
        dpg.add_button("Dividend Calculator", callback=dividend_calculator)
        dpg.add_same_line()
        dpg.add_input_text("Ticker", label='')
        dpg.end()
        dpg.start_dearpygui(primary_window="Main Window")

primary()

