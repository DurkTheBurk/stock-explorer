
import dearpygui.core as dpg
import dearpygui.simple as sdpg
import time

class datas():
    def __init__(self):
        self.value = ''
        self.graph = []
        self.shares = 0
        self.title = "Calculator $"+dpg.get_value('Ticker')
        self.earnings = 0

    def update_graph(self, sender, data):
        self.value = dpg.get_value(self.title+'cost') * dpg.get_value(self.title +'shares')
        self.graph = [self.value]
        self.shares = dpg.get_value(self.title+'shares')
        for month, year in enumerate(range(1, (dpg.get_value(self.title +'years') * 12)  + 1)):
            if not month % dpg.get_value(self.title +'frequency'):
                self.value += (self.shares * dpg.get_value(self.title +'yeild'))
                self.shares =self. value / dpg.get_value(self.title +'cost')
            self.value +=  self.value * (dpg.get_value(self.title +'growth') / 100) / 12
            self.graph.append(self.value)

        dpg.clear_plot(self.title+' Graph')
        dpg.add_line_series(plot=self.title+' Graph', name=dpg.get_value('Ticker') + ' Graph', x=[i for i in range(1, len(self.graph) + 1)], y=self.graph)
        dpg.set_value(self.title + ' earnings', value=f"Gain {round(self.value-(dpg.get_value(self.title+'cost') * dpg.get_value(self.title +'shares')), 2)} Total {round(self.value, 2)}")

def dividend_calculator(sender, data):
    D = datas()
    with sdpg.window(D.title, width=512, height=512):
        dpg.add_separator()
        dpg.add_input_int(name=D.title+'years', default_value=10, label='Years', width=250)
        dpg.add_input_float(name=D.title+'shares', default_value=8800, label='Shares', width=250)
        dpg.add_input_float(name=D.title+'cost', default_value=3.78, label='Cost', width=250)
        dpg.add_input_float(name=D.title+'yeild', default_value=0.075, label='Yeild', width=250)
        dpg.add_input_int(name=D.title+'frequency', default_value=4, label='Frequency', width=250)
        dpg.add_input_int(name=D.title+'growth', default_value=4, label='Growth', width=250)
        dpg.add_button(D.title+"Run", callback=D.update_graph, label="Run")
        dpg.add_text(D.title + ' earnings', default_value='Earnings 0')
        dpg.add_plot(D.title +' Graph', height=-1, width=-1)
        #dpg.add_line_series(title + ' Graph', title + ' Graph', x=[i for i in range(len(graph))], y=graph)
